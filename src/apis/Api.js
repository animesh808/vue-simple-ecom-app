import axios from "axios";

const Api = axios.create({
    baseURL: 'http://laravel-api.com/api/'
});

export default Api;