export const cartTotalAmount = (state) => {
    var sum = 0
    state.cart.forEach(item => {
        sum += item.product.price * item.quantity; 
    });
    return sum;
}

export const cartItemCount = (state) => {
    return state.cart.length;
}