export const SET_CART = (state, cartData) => {
    state.cart = cartData;
}

export const ADD_TO_CART = (state, {product,quantity}) => {
    let productInCart = state.cart.find(item => {
        return item.product.id === product.id
    })

    if(productInCart){
        productInCart.quantity += quantity;
        return;
    }

    state.cart.push({
        product,
        quantity
    })
}

export const REMOVE_ITEM_FROM_CART = (state, product) => {
    //console.log(product);
    state.cart = state.cart.filter(item => {
        return item.product.id != product.id
    })
}

export const CLEAR_ITEMS_FROM_CART = (state) => {
    state.cart = []
}