import Cart from "../../../apis/Cart";

export const addToCart = ({ commit,dispatch }, {product,quantity}) => {
    commit('ADD_TO_CART', {product,quantity})
    dispatch('addNotification', {
        type: 'success',
        message: "Product added successfully!!!"
    }, {root: true})
    Cart.store({product_id: product.id, quantity: quantity})
}

export const getCartItems = ({commit}) => {
    Cart.all().then(response => {
        commit('SET_CART', response.data);
    })
}

export const removeItemFromCart = ({commit, dispatch}, product) => {
    commit('REMOVE_ITEM_FROM_CART', product)
    dispatch('addNotification', {
        type: 'success',
        message: "Product removed from cart!!"
    }, {root: true})
    Cart.remove(product.id)
}

export const clearItemsFromCart = ({commit, dispatch}) =>{
    commit('CLEAR_ITEMS_FROM_CART')
    dispatch('addNotification', {
        type: 'success',
        message: "Your cart is now empty!"
    }, {root: true})
    Cart.removeAll()
}